## Go University

Este proyecto es una aplicación web  para la divulgación de contenidos institucionales, académicos y culturales de la Universidad de Córdoba - Colombia. Mediante esta aplicación web ( Go University), se busca mediar la participación, la motivación, la creatividad y mejorar los lazos comunicativos entre los miembros de la comunidad educativa (Universidad de Córdoba - Colombia).

### Tecnologias usadas

#### BackEnd 
* Nodejs
* Express
* Mongoose
* bcrypt
* jsonwebtoken
* mongodb 
* git 
* Postman

#### FrontEnd
* Vue.js
* Vuex 
* Axios
* vue-router  
* Sass 

