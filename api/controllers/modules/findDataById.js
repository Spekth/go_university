

getDataById=(req,res,model)=>{
          let {id}=req.params;
           let field = model.field 
           let populate = model.populate
    model.name.find(field)
    .populate(populate)
    .exec((err,datadb)=>{
       if(err){
           res.status(500).json({
               message:`Error al mostrar la data ${err}`
           })
       }
       if(!datadb){
           res.status(404).json({
               message:'No existe el la data'
           })
       }else{
           res.json(datadb)
       }
    })
};

module.exports={
    getDataById
}