# Go University API 

Esta API fue el primer proyecto que realice a nivel profesional, fueron muchos los retos que afronte pero que pude solucionar. El código fuente esta escrito en `NodeJS`, y la base de datos esta construida con `MongoDB`. Para desarrollar esta API tuve asesoría de un docente de la universidad de  Córdoba - Montería Colombia, que es desarrollador FullStack, https://github.com/jucagi, el cual me guío durante todo el proceso y del que puede aprender muchas cosas como desarrollador BackEnd. También trabaje en conjunto como un compañero cuyo rol era de desarrollador Frontend https://github.com/anuidev8. El se encargo de desarrollar parte del FrontEnd de este proyecto.

## Vistas del proyecto

https://xd.adobe.com/view/ea374f41-325c-41fa-bdd2-3ee078e34ddf-bf64/

## Ejecutar

```
npm install
```


