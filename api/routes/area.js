const express=require('express');
const areaController = require('../controllers/AreaController') ;
/*============================================
    Importamos el archivo de authentication
=========================================== */
const auth = require('../middlewares/auth')
// Importamos el archivo para verficar rol admin;
const router=express.Router();

router.post('/area',[auth.verificaToken],areaController.addArea)
router.get('/areas',[auth.verificaToken],areaController.getAreas)

module.exports=router; 